import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  @Input() type: string;
  @Input() number: string;

  constructor() { }

  productsList: Array<any> = [
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/c0dae9/000000/236x276.png", backgroundColor: "grey", price: "155.00", hot: false},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/f0f0f0/000000/236x276.png", backgroundColor: "blue", price: "155.00", hot: false},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/1e191d/ff4500/236x276.png", backgroundColor: "black", price: "155.00", hot: true},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/f0f0f0/000000/236x276.png", backgroundColor: "grey", price: "155.00", hot: false},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/c0dae9/000000/236x276.png", backgroundColor: "grey", price: "155.00", hot: false},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/1e191d/ff4500/236x276.png", backgroundColor: "black", price: "155.00", hot: true},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/c0dae9/000000/236x276.png", backgroundColor: "blue", price: "155.00", hot: false},
    {id: 1, label: "Picklesuit", vendorName: "John Doelson", url: "http://placehold.jp/f0f0f0/808080/236x276.png", backgroundColor: "grey", price: "155.00", hot: false},
  ];

  categoriesList: Array<any> = [
    {id: 1, label: "Outdoors", url: "http://placehold.jp/200x200.png", backgroundColor: "grey"},
    {id: 2, label: "Indoors", url: "http://placehold.jp/200x200.png", backgroundColor: "blue"}
  ];
  
  ngOnInit() {
    if (this.type === 'highlighted') {
      
    }
  }

}
